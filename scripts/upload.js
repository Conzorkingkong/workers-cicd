const fs = require("fs");
const axios = require("axios");

const WORKER = fs.readFileSync("./dist/main.js");


// First, we upload our Worker to Cloudflare
axios.put(`https://api.cloudflare.com/client/v4/zones/${process.env.ZONE_ID}/workers/script`,
  WORKER,
  {
    headers: {
      'X-Auth-Email': process.env.EMAIL,
      'X-Auth-Key': process.env.API_KEY,
      'Content-Type': "application/javascript"
    }
})
.then(result => {
  console.log("WORKER UPLOADED");
})
.catch(err => {
  console.log("ERROR: " + err);
})

const WORKERPATTERN = {pattern: process.env.PATTERN, enabled: true}
//Then, we configure what URL we want the Worker to be listening on
axios.put(`https://api.cloudflare.com/client/v4/zones/${process.env.ZONE_ID}/workers/script`,
  JSON.stringify(WORKERPATTERN),
  {
    headers: {
      'X-Auth-Email': process.env.EMAIL,
      'X-Auth-Key': process.env.API_KEY,
      'Content-Type': "application/javascript"
    }
})
.then(result => {
  console.log("WORKER UPLOADED");
})
.catch(err => {
  console.log("ERROR: " + err);
})